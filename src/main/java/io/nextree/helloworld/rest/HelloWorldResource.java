package io.nextree.helloworld.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/helloworld")
public class HelloWorldResource {
    //

    @Value("${hostname:localhost}")
    String hostName;

    @GetMapping
    public String helloWorld() {
        //
        System.out.println("helloword is called-------------------------------");
        return "Hello world!";
    }

    @GetMapping("/hostname")
    public String getHostName(){
        //
        return "You've hit " + hostName;
    }
}
