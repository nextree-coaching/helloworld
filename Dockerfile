FROM java:8
ADD build/libs/helloworld-0.0.1-SNAPSHOT.jar app.jar
RUN chmod +x app.jar

ENTRYPOINT ["java",  "-jar","/app.jar"]
