package io.nextree.helloworld.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@Slf4j
@RestController
@RequestMapping("/healthCheck")
public class TestResource {
    //
    @GetMapping()
    public String healthCheck(HttpServletRequest request) {
        //
        log.info("remote addr {}", request.getRemoteAddr());
        log.info("remote host {}", request.getRemoteHost());
        log.info("remote user {}", request.getRemoteUser());
        log.info("request url {}", request.getRequestURL());
        log.info("request uri {}", request.getRequestURI());
        log.info("requested session id {}", request.getRequestedSessionId());
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            log.info("header {} {}", headerName, request.getHeader(headerName));
        }
        return "OK";
    }
}
